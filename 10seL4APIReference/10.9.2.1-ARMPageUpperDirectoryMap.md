### 10.9.2  seL4_ARM_PageUpperDirectory

#### 10.9.2.1  Map

static inline int seL4_ARM_PageUpperDirectory_Map

映射一个Upper页目录。

类型 | 名字 | 描述
--- | --- | ---
seL4_ARM_PageUpperDirectory | _service | 要操作的Upper页目录能力句柄。自当前线程根CNode按机器字位数解析，下同
seL4_CPtr | vspace | 要映射到的VSapce一级页表能力句柄，必须已经在ASID池中赋值
seL4_Word | vaddr | 要映射到的虚拟地址
seL4_ARM_VMAttributes | attr | 虚拟内存页属性，可能的值见第7章

*返回值*：返回0表示成功，非0值表示有错误发生。第10.1节描述了发生错误时消息寄存器和消息标签有关内容。

*描述*：将一个Upper页目录(二级页表)映射一级页表。如果Upper页目录已经映射，则操作失败并返回seL4_InvalidCapability错误；如果映射项已经占用，则操作失败并返回seL4_DeleteFirst错误。
